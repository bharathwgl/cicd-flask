FROM debian:jessie-slim
FROM python:3.9-slim-bullseye

RUN  apt-get update && apt-get install -y \
    git 

#RUN python3 -m venv /opt/venv

RUN python3 -m pip install -U pip
#RUN pip install --upgrade pip
#RUN /usr/bin/pip3 install --upgrade setuptools
RUN python3 -m pip install gunicorn Flask 
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
WORKDIR /app
COPY . /app